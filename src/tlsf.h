/* Two Levels Segregate Fit memory allocator (TLSF)
 * Version 2.4.6
 *
 * Modified for Tuplr by David Robillard.
 * Original TLSF code available at http://rtportal.upv.es/rtmalloc/
 *
 * Written by Miguel Masmano Tello <mimastel@doctor.upv.es>
 *
 * Thanks to Ismael Ripoll for his suggestions and reviews
 *
 * Copyright (C) 2008, 2007, 2006, 2005, 2004
 *
 * This code is released using a dual license strategy: GPL/LGPL
 * You can choose the licence that better fits your requirements.
 *
 * Released under the terms of the GNU General Public License Version 2.0
 * Released under the terms of the GNU Lesser General Public License Version 2.1
 */

#ifndef _TLSF_H_
#define _TLSF_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tlsf_s tlsf_t;

extern tlsf_t* tlsf_init(void* area, size_t area_size);
extern void    tlsf_destroy(tlsf_t* tlsf);
extern size_t  tlsf_add_area(tlsf_t* tlsf, void* area, size_t size);

extern size_t tlsf_get_used_size(tlsf_t* tlsf);
extern size_t tlsf_get_max_size(tlsf_t* tlsf);

extern void*  tlsf_malloc(tlsf_t* tlsf, size_t size);
extern void   tlsf_free(tlsf_t* tlsf, void* ptr);
extern void*  tlsf_realloc(tlsf_t* tlsf, void* ptr, size_t new_size);
extern void*  tlsf_calloc(tlsf_t* tlsf, size_t nelem, size_t elem_size);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
