Resp
====

Resp is an experimental/toy compiler for a syntactically Scheme-like language
with strong inferred typing and native code performance for numerical code.

 -- David Robillard <d@drobilla.net>
