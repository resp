(define-type (Expr)
  (Symbol Symbol)
  (Int Int)
  (List Expr Expr)
  (Empty))

(define list (quote (2 a b c)))

(define (len l)
  (match l
    (Symbol s)
    1

    (Int i)
    1

    (List h t)
    (+ 1 (len t))
    
    (Empty)
    0))

(len list)