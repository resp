(define (eq x y) (= x y))

(eq 1 2)
(eq 1 1)
(eq 10.0 20.0)
(eq 10.0 10.0)
