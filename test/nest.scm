(define (f x)
  (define (g y)
    (* y 2))
  (g (+ x 1)))

(f 3)
