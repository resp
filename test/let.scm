(define-syntax let
  (syntax-rules ()
    ((let ((name val) ...) body1 body2 ...)
     ((lambda (name ...) body1 body2 ...)
      val ...))))

(define inc
  (let ((dy 1))
    (lambda (y) (+ y dy))))

(inc 41)
