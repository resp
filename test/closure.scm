(define (multiplier factor)
  (lambda (x) (* (+ x 0) factor)))

(define doubler (multiplier 2))

(doubler 3)
