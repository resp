(define-syntax and
  (syntax-rules ()
    ((and) #t)
    ((and test) test)
    ((and test1 test2 ...)
     (if test1 (and test2 ...) #f))))

(and (= 1 1) (= 2 2) (= 3 3) (= 42 24))
