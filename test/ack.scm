(define (ack m n)
  (if (= 0 m) (+ n 1)
      (= 0 n) (ack (- m 1) 1)
      (ack (- m 1) (ack m (- n 1)))))

(ack 3 10)

