; A Shape is either a Circle (w/ radius) or a Rectangle (w/ width/height)
(define-type (Shape)
  (Circle Float)
  (Rectangle Float Float))

; Return the area of s
(define (area s)
  (match s
    (Rectangle w h) (* w h)
    (Circle r)      (* 3.14159 r)))

(define s (Rectangle 3.0 4.0))

(area s)