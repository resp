" Vim indent file
" Language:	Resp
" Maintainer:	Dave Robillard <d@drobilla.net>
" Last Change:	2010 Dec 1

" Only load this indent file when no other was loaded.
if exists("b:did_indent")
  finish
endif

setlocal lisp
setlocal lispwords=def,fn,match,def-type
setlocal expandtab

runtime! indent/lisp.vim
